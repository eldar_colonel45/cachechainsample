﻿using CacheChainSample;

public abstract class BaseChainedCache<T>
{
    protected IChainedCache<T> _nextCache { get; set; }

    public BaseChainedCache(IChainedCache<T> nextCache)
    {
        _nextCache = nextCache;
    }

    public BaseChainedCache() { }

    // Retrieve an item from the next cache in the chain, if any
    protected T GetNext(string key)
    {
        if (_nextCache != null) return _nextCache.Get(key);
        return default(T);
    }

    // Add an item to the next cache in the chain, if any
    protected void AddNext(T o, string key)
    {
        if (_nextCache != null)
        {
            _nextCache.Add(o, key);
        }
    }
}
