﻿using CacheChainSample;
using System;
using System.Collections.Generic;

public class LongTermCache<T> : BaseChainedCache<T>, IChainedCache<T>
{
    private Dictionary<string, T> _storage = new Dictionary<string, T>();

    // Either constructor is fine here, we don't care
    public LongTermCache(IChainedCache<T> nextCache) : base(nextCache)
    {

    }

    public LongTermCache() { }

    public void Add(T o, string key)
    {
        _storage.Add(key, o);
        AddNext(o, key);
    }

    public T Get(string key)
    {
        // Check our storage
        if (_storage.ContainsKey(key))
        {
            Console.WriteLine("long term cache retrieval");
            return _storage[key];
        }

        return GetNext(key);
    }
}
