﻿namespace CacheChainSample
{
    public interface IChainedCache<T>
    {
        T Get(string key);

        void Add(T o, string key);
    }
}
