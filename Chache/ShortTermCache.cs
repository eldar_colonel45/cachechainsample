﻿using System;
using CacheChainSample;
using System.Collections.Generic;

public class ShortTermCache<T> : BaseChainedCache<T>, IChainedCache<T>
{
    private Dictionary<string, TimedCacheItem<T>> _storage = new Dictionary<string, TimedCacheItem<T>>();

    public ShortTermCache(IChainedCache<T> nextCache) : base(nextCache) { }

    public ShortTermCache() { }

    public void Add(T o, string key)
    {
        _storage.Add(key, new TimedCacheItem<T>(o, DateTime.Now.AddSeconds(1)));
        AddNext(o, key);
    }

    public T Get(string key)
    {
        if (_storage.ContainsKey(key) && !_storage[key].IsExpired)
        {
            return _storage[key].Data;
        }

        return GetNext(key);
    }
}