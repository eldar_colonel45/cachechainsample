﻿using System;

public class TimedCacheItem<T>
{
    public TimedCacheItem(T data, DateTime absoluteExpiry)
    {
        Data = data;
        Expiry = absoluteExpiry;
    }

    public T Data { get; set; }

    public DateTime Expiry { get; set; }

    public bool IsExpired
    {
        get
        {
            return Expiry == null || Expiry < DateTime.Now;
        }
    }
}
