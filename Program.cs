﻿using System;
using System.Threading;

namespace CacheChainSample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Construct our cache chain
            var cacheChain = new ShortTermCache<string>(new LongTermCache<string>());

            // Store our object 	
            cacheChain.Add("cacheData", "mycachekey");

            // Wait just over 1 second
            Thread.Sleep(1001);

            // Retrieve our item
            Console.WriteLine(cacheChain.Get("mycachekey"));
            Console.ReadKey();
        }
    }
}
